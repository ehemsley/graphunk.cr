require "../spec_helper"
require "../../src/graphunk/classes/data_structures/disjoint_set"

def with_disjoint_set
  yield Graphunk::DisjointSet(String).new
end

describe Graphunk::DisjointSet do
  describe "make_set" do
    with_disjoint_set do |disjoint_set|
      disjoint_set.make_set("3")

      it "adds value to members" do
        disjoint_set.find("3").should eq "3"
      end
    end
  end

  describe "find" do
    with_disjoint_set do |disjoint_set|
      disjoint_set.make_set("3")
      disjoint_set.make_set("4")
      disjoint_set.union("3", "4")

      it "finds the parent" do
        disjoint_set.find("3").should eq "4"
      end
    end
  end

  describe "union" do
    with_disjoint_set do |disjoint_set|
      disjoint_set.make_set("1")
      disjoint_set.make_set("2")
      disjoint_set.make_set("3")
      disjoint_set.make_set("4")
      disjoint_set.union("1", "2")
      disjoint_set.union("3", "4")

      it "groups the sets" do
        disjoint_set.find("1").should eq "2"
        disjoint_set.find("2").should eq "2"
        disjoint_set.find("3").should eq "4"
        disjoint_set.find("4").should eq "4"
      end
    end
  end
end
