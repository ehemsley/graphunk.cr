require "spec"

module Spec
  struct MatchArrayExpectation(T)
    def initialize(@expected_value : Array(T))
    end

    def match(actual_value : Array(T))
      actual_value.sort == @expected_value.sort
    end

    def failure_message(actual_value)
      "Expected: #{actual_value.inspect}\nto match: #{@expected_value.inspect}"
    end

    def negative_failure_message(actual_value)
      "Expected: #{actual_value.inspect}\nto not match: #{@expected_value.inspect}"
    end
  end

  module Expectations
    def match_array(value)
      Spec::MatchArrayExpectation.new value
    end
  end
end
