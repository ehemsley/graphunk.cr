require "../spec_helper"
require "../../src/graphunk/classes/graphs/weighted_directed_graph"

def with_weighted_directed_graph
  yield Graphunk::WeightedDirectedGraph(String).new(
    ["a", "b", "c", "d", "e"],
    { {"a", "b"} => 3, {"a", "c"} => 6, {"a", "e"} => 6, {"b", "c"} => 2, {"b", "d"} => 7, {"c", "d"} => 3, {"d", "a"} => 4 }
  )
end

describe Graphunk::WeightedDirectedGraph do
  describe "add_edge" do
    context "vertices exist and edge does not exist" do
      with_weighted_directed_graph do |graph|
        graph.add_edge("b", "e", 3)

        it "defines the edge" do
          graph.edges.includes?({"b", "e"}).should be_true
        end

        it "defines the weight" do
          graph.edge_weight("b", "e").should eq 3
        end
      end
    end

    context "one of the vertices does not exist" do
      it "raises an ArgumentError" do
        with_weighted_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "z", 3)
          end
        end
      end
    end

    context "the edge already exists" do
      it "raises an ArgumentError" do
        with_weighted_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "b", 3)
          end
        end
      end
    end
  end

  describe "remove_edge" do
    context "the edge exists" do
      with_weighted_directed_graph do |graph|
        graph.remove_edge("a", "b")

        it "removes edge" do
          graph.edges.includes?({"a", "b"}).should be_false
        end

        it "removes weight" do
          graph.weights.[{"a", "b"}]?.should be_nil
        end
      end
    end

    context "the edge does not exist" do
      it "raises an ArgumentError" do
        with_weighted_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("y", "z")
          end
        end
      end
    end
  end

  describe "edge_weight" do
    with_weighted_directed_graph do |graph|
      context "the edge exists" do
        it "returns the weight of the given edge" do
          graph.edge_weight("a", "b").should eq 3
        end
      end

      context "edge does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.edge_weight("a", "z")
          end
        end
      end
    end
  end

  describe "adjust_weight" do
    context "the edge exists" do
      it "adjusts the value of the weight" do
        with_weighted_directed_graph do |graph|
          graph.adjust_weight("a", "b", 1)
          graph.edge_weight("a", "b").should eq 1
        end
      end
    end

    context "the edge does not exist" do
      it "raises an ArgumentError" do
        with_weighted_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.adjust_weight("y", "z", 2)
          end
        end
      end
    end
  end

  describe "shortest_path_distance" do
    with_weighted_directed_graph do |graph|
      context "start vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.shortest_path_distance("z", "a")
          end
        end
      end

      context "end vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.shortest_path_distance("a", "z")
          end
        end
      end

      context "using dijkstra" do
        context "path exists" do
          it "returns the shortest path distance" do
            graph.shortest_path_distance("a", "d").should eq 8
          end
        end

        context "path does not exist" do
          no_path_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => 2, {"c", "a"} => 1 }
          )

          it "returns nil" do
            no_path_graph.shortest_path_distance("a", "d").should be_nil
          end
        end

        context "negative edge weight exists" do
          negative_weight_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => -2, {"c", "d"} => 2 }
          )

          it "raises error" do
            expect_raises Graphunk::NegativeWeightException do
              negative_weight_graph.shortest_path_distance("a", "b")
            end
          end
        end
      end

      context "with bellman-ford" do
        context "path exists" do
          it "returns the shortest path distance" do
            graph.shortest_path_distance("a", "d", Graphunk::Algorithms::BellmanFord).should eq 8
          end
        end

        context "graph contains negative weight" do
          negative_weight_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"a", "c"} => 2, {"b", "d"} => -1, {"c", "d"} => 1 }
          )

          it "returns the shortest path distance" do
            negative_weight_graph.shortest_path_distance("a", "d", Graphunk::Algorithms::BellmanFord).should eq 1
          end
        end

        context "path does not exist" do
          no_path_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => 2, {"c", "a"} => 1 }
          )

          it "returns nil" do
            no_path_graph.shortest_path_distance("a", "d", Graphunk::Algorithms::BellmanFord).should be_nil
          end
        end

        context "negative cycle exists" do
          negative_cycle_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => -2, {"b", "c"} => -1, {"c", "a"} => -1, {"c", "d"} => 2 }
          )

          it "raises negative cycle exception" do
            expect_raises Graphunk::NegativeCycleException do
              negative_cycle_graph.shortest_path_distance("a", "d", Graphunk::Algorithms::BellmanFord)
            end
          end
        end
      end
    end
  end

  describe "shortest_path" do
    with_weighted_directed_graph do |graph|
      context "start vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.shortest_path("z", "d")
          end
        end
      end

      context "end vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.shortest_path("a", "z")
          end
        end
      end

      context "dijkstra" do
        context "path exists" do
          it "returns the shortest path" do
            graph.shortest_path("a", "d").should eq ["a", "b", "c", "d"]
          end
        end

        context "path does not exist" do
          no_path_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => 2, {"c", "a"} => 1 }
          )

          it "returns nil" do
            no_path_graph.shortest_path("a", "d").should be_nil
          end
        end

        context "graph contains negative weight" do
          negative_weight_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => -2, {"c", "d"} => 2 }
          )

          it "raises error" do
            expect_raises Graphunk::NegativeWeightException do
              negative_weight_graph.shortest_path("a", "d")
            end
          end
        end
      end

      context "bellman-ford" do
        context "path exists" do
          it "returns the shortest path" do
            graph.shortest_path("a", "d", Graphunk::Algorithms::BellmanFord).should eq ["a", "b", "c", "d"]
          end
        end

        context "graph contains negative weight" do
          negative_weight_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"a", "c"} => 1, {"b", "d"} => -2, {"c", "d"} => 2 }
          )

          it "returns the shortest path" do
            negative_weight_graph.shortest_path("a", "d", Graphunk::Algorithms::BellmanFord).should eq ["a", "b", "d"]
          end
        end

        context "path does not exist" do
          no_path_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => 2, {"b", "c"} => 2, {"c", "a"} => 1 }
          )

          it "returns nil" do
            no_path_graph.shortest_path("a", "d", Graphunk::Algorithms::BellmanFord).should be_nil
          end
        end

        context "path contains negative cycle" do
          negative_cycle_graph = Graphunk::WeightedDirectedGraph(String).new(
            ["a", "b", "c", "d"],
            { {"a", "b"} => -2, {"b", "c"} => -1, {"c", "a"} => -1, {"c", "d"} => 2 }
          )

          it "raises negative cycle exception" do
            expect_raises Graphunk::NegativeCycleException do
              negative_cycle_graph.shortest_path("a", "d", Graphunk::Algorithms::BellmanFord)
            end
          end
        end
      end
    end
  end
end
