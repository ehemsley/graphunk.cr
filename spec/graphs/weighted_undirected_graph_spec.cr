require "../spec_helper"
require "../../src/graphunk/classes/graphs/weighted_undirected_graph"

def with_weighted_undirected_graph
  yield Graphunk::WeightedUndirectedGraph(String).new(
    ["a", "b", "c", "d"],
    { {"a", "b"} => 2, {"a", "c"} => 4, {"b", "c"} => 8, {"b", "d"} => 5 }
  )
end

describe Graphunk::WeightedUndirectedGraph do
  describe "remove_vertex" do
    context "the vertex exists" do
      with_weighted_undirected_graph do |graph|
        graph.remove_vertex("c")

        it "removes edges" do
          graph.edges.should match_array [{"a", "b"}, {"b", "d"}]
        end

        it "removes vertices" do
          graph.vertices.should match_array ["a", "b", "d"]
        end

        it "removes weights" do
          graph.weights.includes?({"a", "c"}).should be_false
          graph.weights.includes?({"b", "c"}).should be_false
        end
      end
    end

    context "the vertex does not exist" do
      it "raises an ArgumentError" do
        with_weighted_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_vertex("f")
          end
        end
      end
    end
  end

  describe "remove_edge" do
    context "the edge exists" do
      with_weighted_undirected_graph do |graph|
        graph.remove_edge("a", "b")

        it "removes edge" do
          graph.edges.should match_array [{"a", "c"}, {"b", "c"}, {"b", "d"}]
        end

        it "removes weight" do
          graph.weights[{"a", "b"}]?.should be_nil
        end
      end
    end

    context "the edge does not exist" do
      it "raises an ArgumentError" do
        with_weighted_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("f", "z")
          end
        end
      end
    end
  end

  describe "add_edge" do
    context "vertices exist and edge does not exist" do
      with_weighted_undirected_graph do |graph|
        graph.add_edge("a", "d", 6)

        it "defines the weight" do
          graph.edge_weight("a", "d").should eq 6
        end

        it "defines the edge" do
          graph.edges.includes?({"a", "d"}).should be_true
        end
      end
    end

    context "one of the vertices does not exist" do
      it "raises an ArgumentError" do
        with_weighted_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "e", 4)
          end
        end
      end
    end

    context "the edge already exists" do
      it "raises an ArgumentError" do
        with_weighted_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "b", 4)
          end
        end
      end
    end
  end

  describe "edge_weight" do
    with_weighted_undirected_graph do |graph|
      context "edge exists" do
        it "gets the weight of the given edge" do
          graph.edge_weight("a", "b").should eq 2
        end
      end

      context "edge does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.edge_weight("z", "f")
          end
        end
      end
    end
  end

  describe "adjust_weight" do
    context "the edge exists" do
      it "adjusts the value in the weight hash" do
        with_weighted_undirected_graph do |graph|
          graph.adjust_weight("a", "b", 1)
          graph.edge_weight("a", "b").should eq 1
        end
      end
    end

    context "the edge does not exist" do
      it "raises an ArgumentError" do
        with_weighted_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.adjust_weight("a", "f", 3)
          end
        end
      end
    end
  end

  # TODO: add some more contextual stuff here (disconnected graphs, etc)
  describe "minimum_spanning_tree" do
    graph = Graphunk::WeightedUndirectedGraph(String).new(
      ["a", "b", "c", "d", "e", "f"],
      { {"a", "b"} => 1, {"a", "d"} => 3, {"b", "c"} => 6, {"b", "d"} => 5, {"b", "e"} => 1, {"c", "e"} => 5, {"c", "f"} => 2, {"d", "e"} => 1, {"e", "f"} => 4 }
    )

    context "using prim's algorithm" do
      it "contains all the vertices in the original graph" do
        graph.minimum_spanning_tree(Graphunk::Algorithms::Prim).vertices.should match_array graph.vertices
      end

      it "contains only the edges in the minimum spanning tree " do
        graph.minimum_spanning_tree(Graphunk::Algorithms::Prim).edges.should match_array [{"a", "b"}, {"b", "e"}, {"c", "f"}, {"d", "e"}, {"e", "f"}]
      end
    end

    context "using kruskal's algorithm" do
      it "contains all the vertices in the original graph" do
        graph.minimum_spanning_tree(Graphunk::Algorithms::Kruskal).vertices.should match_array graph.vertices
      end

      it "contains only the edges in the minimum spanning tree " do
        graph.minimum_spanning_tree(Graphunk::Algorithms::Kruskal).edges.should match_array [{"a", "b"}, {"b", "e"}, {"c", "f"}, {"d", "e"}, {"e", "f"}]
      end
    end
  end
end
