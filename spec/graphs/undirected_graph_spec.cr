require "../spec_helper"
require "../../src/graphunk/classes/graphs/undirected_graph"

def with_undirected_graph
  yield Graphunk::UndirectedGraph(String).new(["a", "b", "c"],
    [{"a", "b"}, {"a", "c"}, {"b", "c"}])
end

describe Graphunk::UndirectedGraph do
  describe "vertices" do
    it "returns a list of all vertices" do
      with_undirected_graph do |graph|
        graph.vertices.should match_array ["a", "b", "c"]
      end
    end
  end

  describe "edges" do
    it "returns a list of all edges" do
      with_undirected_graph do |graph|
        graph.edges.should match_array [{"a", "b"}, {"a", "c"}, {"b", "c"}]
      end
    end
  end

  describe "add_vertex" do
    context "vertex does not exist" do
      it "adds a vertex to the graph" do
        with_undirected_graph do |graph|
          graph.add_vertex("d")
          graph.vertices.should match_array ["a", "b", "c", "d"]
        end
      end
    end

    context "vertex exists" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises(ArgumentError) do
            graph.add_vertex("a")
          end
        end
      end
    end
  end

  describe "add_vertices" do
    context "vertices do not exist" do
      it "adds the vertices to the graph" do
        with_undirected_graph do |graph|
          graph.add_vertices("g", "h", "i")
          graph.vertices.should match_array ["a", "b", "c", "g", "h", "i"]
        end
      end
    end

    context "one of the vertices exists in the graph" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises(ArgumentError) do
            graph.add_vertices("g", "h", "a")
          end
        end
      end
    end
  end

  describe "add_edge" do
    context "vertices exist" do
      it "adds an edge to the graph" do
        with_undirected_graph do |graph|
          graph.add_vertex("d")
          graph.add_edge("c", "d")
          graph.edges.should match_array [{"a", "b"}, {"a", "c"}, {"b", "c"}, {"c", "d"}]
        end
      end
    end

    context "one of the vertices does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "d")
          end
        end
      end
    end
  end

  describe "remove_edge" do
    context "edge exists" do
      it "removes an edge from the graph" do
        with_undirected_graph do |graph|
          graph.remove_edge("b", "c")
          graph.edges.should match_array [{"a", "b"}, {"a", "c"}]
        end
      end
    end

    context "one of the vertices does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("c", "d")
          end
        end
      end
    end

    context "edge does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("b", "c")
            graph.remove_edge("b", "c")
          end
        end
      end
    end
  end

  describe "remove_vertex" do
    context "vertex exists" do
      it "removes a vertex from the graph" do
        with_undirected_graph do |graph|
          graph.remove_vertex("b")
          graph.vertices.should match_array ["a", "c"]
        end
      end

      it "removes edges containing the vertex from the graph" do
        with_undirected_graph do |graph|
          graph.remove_vertex("b")
          graph.edges.should eq [{"a", "c"}]
        end
      end
    end

    context "vertex does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_vertex("f")
          end
        end
      end
    end
  end

  describe "edges_on_vertex" do
    context "vertex exists" do
      it "returns a list of all edges containing the input vertex" do
        with_undirected_graph do |graph|
          graph.edges_on_vertex("a").should match_array [{"a", "b"}, {"a", "c"}]
        end
      end
    end

    context "vertex does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.edges_on_vertex("d")
          end
        end
      end
    end
  end

  describe "neighbors" do
    context "vertex exists" do
      it "returns a list of all neighbor vertices of the input vertex" do
        with_undirected_graph do |graph|
          graph.neighbors("a").should match_array ["b", "c"]
        end
      end
    end

    context "vertex does not exist" do
      it "raises an error if the input vertex does not exist" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.neighbors("d")
          end
        end
      end
    end
  end

  describe "edge_exists?" do
    context "edge exists" do
      it "returns true" do
        with_undirected_graph do |graph|
          graph.edge_exists?("a", "b").should be_true
        end
      end
    end

    context "edge does not exist" do
      it "returns false" do
        with_undirected_graph do |graph|
          graph.remove_edge("b", "c")
          graph.edge_exists?("b", "c").should be_false
        end
      end
    end

    context "an input vertex does not exist" do
      it "returns false" do
        with_undirected_graph do |graph|
          graph.edge_exists?("b", "d").should be_false
        end
      end
    end
  end

  describe "vertex_exists?" do
    context "vertex exists" do
      it "returns true" do
        with_undirected_graph do |graph|
          graph.vertex_exists?("a").should be_true
        end
      end
    end

    context "vertex does not exist" do
      it "returns false" do
        with_undirected_graph do |graph|
          graph.vertex_exists?("f").should be_false
        end
      end
    end
  end

  describe "lexicographic_bfs" do
    it "returns a lexicographic ordering on the graph" do
      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e"],
        [{"a", "b"}, {"a", "c"}, {"b", "c"}, {"b", "d"}, {"b", "e"}, {"c", "d"}, {"d", "e"}])

      graph.lexicographic_bfs.should eq ["a", "b", "c", "d", "e"]
    end
  end

  describe "chordal?" do
    graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e"],
      [{"a", "b"}, {"a", "c"}, {"b", "c"}, {"b", "d"}, {"b", "e"}, {"c", "d"}, {"d", "e"}])

    context "graph is chordal" do
      it "returns true" do
        graph.chordal?.should be_true
      end
    end

    context "graph is not chordal" do
      it "returns false" do
        graph.remove_edge("b", "c")
        graph.chordal?.should be_false
      end
    end
  end

  describe "clique?" do
    graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e"],
      [{"a", "b"}, {"a", "c"}, {"b", "c"}, {"b", "d"}, {"b", "e"}, {"c", "d"}, {"d", "e"}])

    context "input vertices are a clique" do
      it "returns true" do
        graph.clique?(["a", "b", "c"]).should be_true
      end
    end

    context "input vertices are not a clique" do
      it "returns false" do
        graph.clique?(["b", "c", "e"]).should be_false
      end
    end
  end

  describe "complete?" do
    context "graph is complete" do
      it "returns true" do
        graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d"],
          [{"a", "b"}, {"a", "c"}, {"a", "d"}, {"b", "c"}, {"b", "d"}, {"c", "d"}])
        graph.complete?.should be_true
      end
    end

    context "graph is not complete" do
      it "returns false" do
        graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d"],
          [{"a", "b"}, {"a", "c"}, {"a", "d"}, {"b", "c"}, {"b", "d"}])
        graph.complete?.should be_false
      end
    end
  end

  describe "bipartite?" do
    context "graph is bipartite" do
      it "returns true" do
        graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e"],
          [{"a", "b"}, {"a", "c"}, {"b", "d"}, {"c", "e"}])

        graph.bipartite?.should be_true
      end
    end

    context "graph is not bipartite" do
      it "returns false" do
        with_undirected_graph do |graph|
          graph.bipartite?.should be_false
        end
      end
    end
  end

  describe "comparability?" do
    context "the graph is a comparability graph" do
      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"a", "g"}, {"b", "c"}, {"c", "d"}, {"d", "e"}, {"d", "f"}, {"e", "f"}, {"f", "g"}])

      it "returns true" do
        graph.comparability?.should be_true
      end
    end

    context "the graph is not a comparability graph" do
      # graph = Graphunk::UndirectedGraph.new({"a" => ["b", "g"], "b" => ["c"], "c" => ["d"], "d" => ["e"], "e" => ["f"], "f" => ["g"], "g" => [] of String})

      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"a", "g"}, {"b", "c"}, {"c", "d"}, {"d", "e"}, {"e", "f"}, {"f", "g"}])

      it "returns false" do
        graph.comparability?.should be_false
      end
    end
  end

  describe "transitive_orientation" do
    context "the graph is a comparability graph" do
      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"a", "g"}, {"b", "c"}, {"c", "d"}, {"d", "e"}, {"d", "f"}, {"e", "f"}, {"f", "g"}])

      it "returns the transitive orientation of the graph" do
        valid_orientation = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
          [{"a", "b"}, {"a", "g"}, {"c", "b"}, {"c", "d"}, {"e", "d"}, {"f", "d"}, {"f", "e"}, {"f", "g"}])
        graph.transitive_orientation.as(Graphunk::DirectedGraph).edges.should match_array valid_orientation.edges
      end
    end

    context "another comparability graph" do
      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d"],
        [{"a", "b"}, {"a", "c"}, {"a", "d"}, {"b", "c"}, {"c", "d"}])

      it "returns the transitive orientation of the graph" do
        valid_orientation = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d"],
          [{"a", "b"}, {"a", "c"}, {"a", "d"}, {"b", "c"}, {"d", "c"}])

        graph.transitive_orientation.as(Graphunk::DirectedGraph).edges.should match_array valid_orientation.edges
      end
    end

    context "the graph is not a comparability graph" do
      graph = Graphunk::UndirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"a", "g"}, {"b", "c"}, {"c", "d"}, {"d", "e"}, {"e", "f"}, {"f", "g"}])

      it "returns nil" do
        graph.transitive_orientation.should be_nil
      end
    end
  end

  describe "degree" do
    context "the vertex exists" do
      it "returns the degree of the vertex" do
        with_undirected_graph do |graph|
          graph.degree("a").should eq 2
        end
      end
    end

    context "the vertex does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.degree("z")
          end
        end
      end
    end
  end

  describe "adjacent_edge" do
    context "the edge exists" do
      it "returns the edges adjacent to the given edge" do
        with_undirected_graph do |graph|
          graph.adjacent_edges("a", "b").should match_array [{"a", "c"}, {"b", "c"}]
        end
      end
    end

    context "the edge does not exist" do
      it "raises an ArgumentError" do
        with_undirected_graph do |graph|
          expect_raises ArgumentError do
            graph.adjacent_edges("x", "y")
          end
        end
      end
    end
  end
end
