require "../../modules/undirected_graph_methods"
require "./directed_graph"

module Graphunk
  class UndirectedGraph(T)
    include Graph(T)
    include UndirectedGraphMethods(T)

    def initialize(@vertices = [] of T, @edges = [] of Tuple(T, T))
      @neighbors = Hash(T, Set(T)).new
      @vertices.each do |vertex|
        @neighbors[vertex] = Set(T).new
      end
      @edges.each do |edge|
        add_neighbors(edge)
      end
    end

    def add_edge(edge)
      if edge_exists?(edge)
        raise ArgumentError.new("This edge already exists")
      elsif vertex_exists?(edge.first) && vertex_exists?(edge.last)
        @edges << edge
        add_neighbors(edge)
      else
        raise ArgumentError.new("One of the vertices referenced does not exist in the graph")
      end
    end

    def add_edge(first_vertex, second_vertex)
      add_edge order_vertices(first_vertex, second_vertex)
    end

    def remove_edge(edge)
      if edge_exists?(edge)
        @edges.delete edge
        @neighbors[edge.first].delete edge.last
        @neighbors[edge.last].delete edge.first
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def remove_edge(first_vertex, second_vertex)
      remove_edge order_vertices(first_vertex, second_vertex)
    end

    def adjacent_edges?(first_edge, second_edge)
      adjacent_edges(first_edge).include? second_edge
    end

    def adjacent_edges(v, u)
      if edge_exists?(v, u)
        edges.select { |edge| edge.includes?(v) || edge.includes?(u) } - [order_vertices(v, u)]
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end
  end
end
