module Graphunk
  # NOTE: tree structure is left-child right-sibling tree

  class PairingHeapNode(K, V)
    def initialize(@key : K, @value : V)
      @prev_node = nil
      @next_node = nil
      @child = nil
    end

    getter :key
    property :value

    def prev_node : PairingHeapNode(K, V)
      return @prev_node.as(PairingHeapNode(K, V))
    end

    def next_node : PairingHeapNode(K, V)
      return @next_node.as(PairingHeapNode(K, V))
    end

    def child : PairingHeapNode(K, V)
      return @child.as(PairingHeapNode(K, V))
    end

    def prev_node=(@prev_node : PairingHeapNode(K, V))
    end

    def next_node=(@next_node : PairingHeapNode(K, V))
    end

    def child=(@child : PairingHeapNode(K, V))
    end

    def add_child(new_child)
      if child?
        new_child.prev_node = self
        new_child.next_node = child
        @child.as(PairingHeapNode(K, V)).prev_node = new_child
      end
      @child = new_child
    end

    def unlink_prev_node
      @prev_node = nil
    end

    def unlink_next_node
      @next_node = nil
    end

    def unlink_child
      @child = nil
    end

    def prev?
      @prev_node != nil
    end

    def next?
      @next_node != nil
    end

    def child?
      @child != nil
    end

    def clone
      new_node = PairingHeapNode(K, V).new(key, value)
      new_node.prev_node = prev_node.clone if prev?
      new_node.next_node = next_node.clone if next?
      new_node.child = child.clone if child?
      new_node
    end
  end

  class PairingHeap(K, V)
    @root : PairingHeapNode(K, V) | Nil

    def initialize(key_value : {K, V} | Nil = nil)
      if key_value.nil?
        @root = nil
      else
        @root = PairingHeapNode(K, V).new(key_value.first, key_value.last)
      end
    end

    def initialize(@root : PairingHeapNode(K, V))
    end

    getter :root

    def empty?
      @root == nil
    end

    def root
      return @root.as(PairingHeapNode(K, V))
    end

    def merge(other : PairingHeapNode(K, V))
      if empty?
        return PairingHeap(K, V).new(other)
      elsif root.value < other.value
        root.add_child(other)
        return PairingHeap(K, V).new(root)
      else
        other.add_child(root)
        return PairingHeap(K, V).new(other)
      end
    end

    def merge!(other : PairingHeapNode(K, V))
      if empty?
        @root = other
      elsif root.value < other.value
        root.add_child(other)
      else
        other.add_child(root)
        @root = other
      end
    end

    def merge(other : PairingHeap(K, V))
      merge(other.root)
    end

    def merge!(other : PairingHeap(K, V))
      merge!(other.root)
    end

    def insert(key : K, value : V)
      merge!(PairingHeapNode(K, V).new(key, value))
    end

    def insert(key_value : {K, V})
      insert(key_value.first, key_value.last)
    end

    # this is fuckin nasty
    def remove(node)
      if node.prev?
        if node.prev_node.next? && node.prev_node.next_node == node #its a sibling
          node.prev_node.next_node = node.next_node if node.next?
        end

        if node.prev_node.child? && node.prev_node.child == node #its a leftmost sibling
          node.prev_node.unlink_child
          if node.next?
            node.prev_node.child = node.next_node
            node.next_node.prev_node = node.prev_node
          end
        end

        node.unlink_prev_node
        node.unlink_next_node
      end

      if node.child?
        subtrees = [] of PairingHeapNode(K, V)

        current_child = node.child
        subtrees << current_child

        while current_child.next?
          next_child = current_child.next_node
          current_child.unlink_next_node
          current_child.unlink_prev_node
          subtrees << next_child
          current_child = next_child
        end

        first_pass_trees = subtrees.each_slice(2).map { |pair| pair.size > 1 ? PairingHeap(K, V).new(pair[0]).merge(pair[1]) : PairingHeap(K,V).new(pair[0]) }.to_a
        final_tree = first_pass_trees.reverse.reduce { |main_tree, meld_tree| main_tree.merge(meld_tree) }

        if node == @root
          @root = final_tree.root
        else
          merge!(final_tree)
        end
      end
    end

    def adjust(node, value)
      remove(node)
      node.value = value
      merge!(node)
    end

    def find_min
      if empty?
        return nil
      else
        {root.key, root.value}
      end
    end

    def delete_min
      if empty?
        raise ArgumentError.new("cant delete from empty tree")
      else
        min = find_min.as({K, V})
        remove(root)
        min
      end
    end
  end
end
