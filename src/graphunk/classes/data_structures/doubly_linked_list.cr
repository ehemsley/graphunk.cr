module Graphunk
  class DoublyLinkedListNode(T)
    @prev_node : DoublyLinkedListNode(T) | Nil
    @next_node : DoublyLinkedListNode(T) | Nil

    def initialize(@list : DoublyLinkedList(T), @value : T)
      @prev_node = nil
      @next_node = nil
    end

    getter :list
    property value : T

    def prev?
      !@prev_node.nil?
    end

    def next?
      !@next_node.nil?
    end

    def prev_node : DoublyLinkedListNode(T)
      if @prev_node.nil?
        raise ArgumentError.new("duhh")
      else
        @prev_node.as(DoublyLinkedListNode(T))
      end
    end

    def next_node : DoublyLinkedListNode(T)
      if @next_node.nil?
        raise ArgumentError.new("fucklkkljk")
      else
        @next_node.as(DoublyLinkedListNode(T))
      end
    end

    def set_prev_node(other : DoublyLinkedListNode(T))
      @prev_node = other
    end

    def set_next_node(other : DoublyLinkedListNode(T))
      @next_node = other
    end

    def unlink_prev_node
      @prev_node = nil
    end

    def unlink_next_node
      @next_node = nil
    end
  end

  #TODO: implement iterator
  class DoublyLinkedList(T)
    @root_node : DoublyLinkedListNode(T) | Nil
    @tail_node : DoublyLinkedListNode(T) | Nil

    def initialize
      @root_node = nil
      @tail_node = nil
      @size = 0
    end

    getter :size

    private def root_node
      raise ArgumentError.new("fuckk") if empty?
      @root_node.as(DoublyLinkedListNode(T))
    end

    private def set_root_node(other : DoublyLinkedListNode(T))
      @root_node = other
    end

    private def unlink_root_node
      @root_node = nil
    end

    private def tail_node
      raise ArgumentError.new("fuckkk") if empty?
      @tail_node.as(DoublyLinkedListNode(T))
    end

    private def set_tail_node(other : DoublyLinkedListNode(T))
      @tail_node = other
    end

    private def unlink_tail_node
      @tail_node = nil
    end

    def empty?
      size == 0
    end

    def first
      raise ArgumentError.new("cant get first of empty") if empty?
      root_node.value
    end

    def last
      raise ArgumentError.new("cant get first of empty") if empty?
      tail_node.value
    end

    def [](index)
      raise IndexError.new("Index out of range") if empty? || index > size-1

      node = root_node
      index.times do
        node = node.next_node
      end
      node.value
    end

    def shift(num)
      raise IndexError.new("Index out of range") if empty? || num > size

      new_list = DoublyLinkedList(T).new
      node = root_node
      num.times do
        node = node.next_node
      end

      while node.next?
        new_list.add(node.value)
        node = node.next_node
      end
      new_list.add(node.value)
      
      new_list
    end

    def shift!(num)
      raise IndexError.new("Index out of range") if empty? || num > size

      node = root_node
      num.times do
        remove(node)
        node = node.next_node
      end
    end

    def clone
      new_list = DoublyLinkedList(T).new
      return new_list if empty?

      node = root_node
      while node.next?
        new_list.add(node.value)
        node = node.next_node
      end
      new_list.add(node.value)

      new_list
    end

    def add(value)
      new_node = DoublyLinkedListNode(T).new(self, value)
      if empty?
        set_root_node(new_node)
        set_tail_node(new_node)
      else
        tail_node.set_next_node(new_node)
        new_node.set_prev_node(tail_node)
        set_tail_node(new_node)
      end

      @size += 1
      new_node
    end

    def remove(node : DoublyLinkedListNode(T))
      raise ArgumentError.new("node does not belong to this list") if node.list != self

      if size == 1
        unlink_root_node
        unlink_tail_node
      elsif node == root_node
        set_root_node(node.next_node)
      elsif node == tail_node
        set_tail_node(node.prev_node)
      else
        node.prev_node.set_next_node(node.next_node)
        node.next_node.set_prev_node(node.prev_node)
      end

      @size -= 1
    end
  end
end
