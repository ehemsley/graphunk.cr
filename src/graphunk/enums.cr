module Graphunk
  enum Algorithms
    Prim
    Kruskal
    Dijkstra
    BellmanFord
  end
end
