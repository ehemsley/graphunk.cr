require "./graph"

module Graphunk
  module DirectedGraphMethods(T)
    include Graph(T)

    def edge_exists?(first_vertex, second_vertex)
      edges.includes?({first_vertex, second_vertex})
    end

    def transpose
      graph = DirectedGraph(T).new
      vertices.each do |vertex|
        graph.add_vertex(vertex)
      end
      edges.each do |edge|
        graph.add_edge(edge.last, edge.first)
      end
      graph
    end

    def transpose!
      edges.map! { |edge| edge.reverse }
    end

    def reachable_by_two_path(start)
      if vertex_exists?(start)
        reached_vertices = neighbors(start)
        reached_vertices.each do |vertex|
          reached_vertices += neighbors(vertex)
        end
        reached_vertices.uniq
      else
        raise ArgumentError.new("That vertex does not exist in the graph")
      end
    end

    def square
      graph = self.clone

      vertices.each do |vertex|
        (reachable_by_two_path(vertex) - [vertex]).each do |reachable|
          graph.add_edge(vertex, reachable) unless edge_exists?(vertex, reachable)
        end
      end

      graph
    end

    def dfs
      discovered = [] of String
      time = 0
      output = {} of T => Hash(Symbol, Int32)
      vertices.each { |vertex| output[vertex] = {} of Symbol => Int32 }

      dfs_helper = uninitialized T -> Void
      dfs_helper = ->(u : T) do # only u can do u, so do it!
        discovered << u
        time += 1
        output[u][:start] = time

        neighbors(u).each do |neighbor|
          dfs_helper.call neighbor unless discovered.includes?(neighbor)
        end

        time += 1
        output[u][:finish] = time
        return nil
      end

      vertices.each do |vertex|
        dfs_helper.call vertex unless discovered.includes?(vertex)
      end

      output
    end

    def topological_sort
      dfs.to_a.sort_by { |vertex, times| times[:finish] }.map { |elt| elt.first }.reverse
    end

    def transitive?
      transitive = true
      vertices.each do |vertex|
        reachable_by_two_edges(vertex).each do |reachable|
          transitive = false unless neighbors(vertex).includes?(reachable)
        end
      end
      transitive
    end

    private def reachable_by_two_edges(start)
      reachable_by_two = [] of T
      reachable_by_one = neighbors(start)
      reachable_by_one.each do |v|
        neighbors(v).each do |u|
          reachable_by_two << u
        end
      end
      reachable_by_two
    end
  end
end
