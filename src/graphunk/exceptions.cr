module Graphunk
  class NegativeWeightException < Exception
  end

  class NegativeCycleException < Exception
  end
end
